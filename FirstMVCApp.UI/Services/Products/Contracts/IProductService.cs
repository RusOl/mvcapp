﻿using System.Collections.Generic;
using FirstMVCApp.UI.Models.Products;

namespace FirstMVCApp.UI.Services.Products.Contracts
{
    public interface IProductService
    {
        List<ProductModel> SearchProducts(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
    }
}