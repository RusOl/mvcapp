﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.UI.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ProductService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<ProductModel> SearchProducts(ProductFilterModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                IEnumerable<Product> products = unitOfWork.Products.GetAllWithCategoriesAndBrands();
                
                products = products
                    .ByPriceFrom(model.PriceFrom)
                    .ByPriceTo(model.PriceTo)
                    .ByName(model.Name)
                    .ByCategoryId(unitOfWork, model.CategoryId)
                    .ByBrandId(unitOfWork, model.BrandId);

                List<ProductModel> productModels = Mapper.Map<List<ProductModel>>(products);
                
                return productModels;
            }
        }

        public ProductCreateModel GetProductCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                var brands = unitOfWork.Brands.GetAll().ToList();

                return new ProductCreateModel()
                {
                    CategoriesSelect = new SelectList(categories, nameof(Category.Id), nameof(Category.Name)),
                    BrandsSelect = new SelectList(brands, nameof(Brand.Id), nameof(Brand.Name))
                };
            }
        }

        public void CreateProduct(ProductCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = Mapper.Map<Product>(model);

                unitOfWork.Products.Create(product);
            }
        }
    }
}
