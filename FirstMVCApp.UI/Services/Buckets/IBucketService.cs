﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Buckets;
using FirstMVCApp.UI.Models.Orders;

namespace FirstMVCApp.UI.Services.Buckets
{
    public interface IBucketService
    {
        Task AddToBucketAsync(int productId, User user);
        public BucketModel GetBucket(User user);
        public void OrderToBucket(BucketModel model, User user);
    }
}
