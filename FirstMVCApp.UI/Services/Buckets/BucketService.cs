﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.StorageGateway;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Buckets;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Buffers;
using Newtonsoft.Json;
using ErrorContext = Newtonsoft.Json.Serialization.ErrorContext;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Services.Buckets
{
    public class BucketService : IBucketService
    {
        public readonly UserManager<User> _userManager;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public BucketService(UserManager<User> userManager,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task AddToBucketAsync(int productId, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var product = unitOfWork.Products.GetById(productId);
                if (product == null)
                    throw new ArgumentOutOfRangeException(nameof(productId), $"No product with id {productId}");

                var bucketItems = string.IsNullOrWhiteSpace(user.Bucket)
                    ? CreateBucket(productId)
                    : UpdateBucket(productId, user.Bucket); 
                

                user.Bucket = JsonConvert.SerializeObject(bucketItems);
                await _userManager.UpdateAsync(user);
            }
        } 
        private List<BucketItem> UpdateBucket(int productId, string currentBucket)
        {
            try
            {
                var currentItems = JsonConvert.DeserializeObject<List<BucketItem>>(currentBucket);
                var existingItem = currentItems.FirstOrDefault(c => c.ProductId == productId);
                if (existingItem != null)
                {
                    existingItem.Qty++;
                }
                else
                {
                    currentItems.Add(
                        new BucketItem()
                        {
                            ProductId = productId,
                            Qty = 1
                        }
                    );
                }

                return currentItems;
            }
            catch
            {
                return CreateBucket(productId);
            }
        }

        private List<BucketItem> CreateBucket(int productId)
        {
            return new List<BucketItem>()
            {
                new BucketItem()
                {
                    ProductId = productId,
                    Qty = 1
                }
            };
        }
        public void OrderToBucket(BucketModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = new Order();
                order.UserId = user.Id;
                order.Sum = model.TotalSum;
                order.DT = DateTime.Now;
                order.Product = JsonConvert.SerializeObject(model.ProductInBucket);
                unitOfWork.Orders.Create(order);
            }
        }
        [Authorize]
        public BucketModel GetBucket(User user)
        {
            BucketModel bucketModel = new BucketModel();
            bucketModel.ProductInBucket = new List<ProductModel>();
            bucketModel.Name = user.UserName;
            using (var unitOfWorkFactory = _unitOfWorkFactory.Create())
            {
                var products = unitOfWorkFactory.Products.GetAllWithCategoriesAndBrands();
                List < ProductModel> pruducts = Mapper.Map<List<ProductModel>>(products);
                string bucket = user.Bucket;
                List<BucketItem> bucketItems = JsonConvert.DeserializeObject<List<BucketItem>>(bucket);
                foreach (var item in bucketItems)
                {
                        ProductModel productModel = pruducts.FirstOrDefault(p => p.Id == item.ProductId);
                        productModel.Qty = item.Qty;
                        productModel.Sum = productModel.Qty * productModel.Price;
                        bucketModel.ProductInBucket.Add(productModel);
                        bucketModel.TotalSum += productModel.Sum;
                }
                    
                return bucketModel;
            }
        }
    }
}
