﻿using FirstMVCApp.UI.Models.Categories;

namespace FirstMVCApp.UI.Services.Categories.Contracts
{
    public interface ICategoryService
    {
        CategoryCreateModel GetCategoryCreateModel();
        void CreateCategory(CategoryCreateModel model);
    }
}
