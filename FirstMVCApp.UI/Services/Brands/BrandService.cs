﻿using System;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Brands;
using FirstMVCApp.UI.Services.Brands.Contracts;

namespace FirstMVCApp.UI.Services.Brands
{
    public class BrandService : IBrandService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BrandService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public BrandCreateModel GetBrandCreateModel()
        {
            return new BrandCreateModel();
        }

        public void CreateBrand(BrandCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = Mapper.Map<Brand>(model);
                unitOfWork.Brands.Create(brands);
            }
        }
    }
}
