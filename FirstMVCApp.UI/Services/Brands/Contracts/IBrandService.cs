﻿using FirstMVCApp.UI.Models.Brands;

namespace FirstMVCApp.UI.Services.Brands.Contracts
{
    public interface IBrandService
    {
        BrandCreateModel GetBrandCreateModel();
        void CreateBrand(BrandCreateModel model);
    }
}
