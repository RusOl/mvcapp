﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Orders;

namespace FirstMVCApp.UI.Services.Orders.Contracts
{
    public interface IOrderService
    {
        public List<OrderModel> GetOrders(User user);
        OrderCreateModel GetOrderCreateModel();
    }
}
