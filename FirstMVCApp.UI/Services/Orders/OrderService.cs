﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Services.Orders.Contracts;
using Microsoft.AspNetCore.Identity;

namespace FirstMVCApp.UI.Services.Orders
{
    public class OrderService : IOrderService
    {
        public readonly UserManager<User> _userManager;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public OrderService(UserManager<User> userManager,
            IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        public List<OrderModel> GetOrders(User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int userId = currentUser.Id;
                var orders = unitOfWork.Orders.GetOrderbyUser(userId).ToList(); 
                var orderModels = Mapper.Map<List<OrderModel>>(orders);
                return orderModels;
            }
        }

        public OrderCreateModel GetOrderCreateModel()
        {
            return new OrderCreateModel();
        }
    }
}
