﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.UI.Models.Orders
{
    public class OrderCreateModel
    {
        [Display(Name = "Наименование")]
        public string Product { get; set; }

        [Display(Name = "User")]
        public string User { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Цена должно быть указана")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Цена не может быть отрицательной")]
        public decimal Sum { get; set; }
    }
}
