﻿namespace FirstMVCApp.UI.Models.Products
{
    public class ProductModel
    {
        public static string NoBrand = "NoName";

        public int Id { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal Sum { get; set; }

    }
}
