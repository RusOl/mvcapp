﻿namespace FirstMVCApp.UI.Models.Products
{
    public class ProductFilterModel
    {
        public string Name { get; set; }
        public int? CategoryId { get; set; }
        public int? BrandId { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
    }
}
