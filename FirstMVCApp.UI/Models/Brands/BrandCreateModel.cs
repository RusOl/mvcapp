﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCApp.UI.Models.Brands
{
    public class BrandCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }
    }
}
