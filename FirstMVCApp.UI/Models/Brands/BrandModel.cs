﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.UI.Models.Brands
{
    public class BrandModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
