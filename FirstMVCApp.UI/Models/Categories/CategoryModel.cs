﻿namespace FirstMVCApp.UI.Models.Categories
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
