﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCApp.UI.Models.Categories
{
    public class CategoryCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }
    }
}
