﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.UI.Models.Products;

namespace FirstMVCApp.UI.Models.Buckets
{
    public class BucketModel
    {
        public string Name { get; set; }
        public List<ProductModel> ProductInBucket { get; set; }
        public decimal TotalSum { get; set; }
    }
}
