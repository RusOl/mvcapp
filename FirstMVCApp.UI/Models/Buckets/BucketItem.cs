﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstMVCApp.UI.Models.Buckets
{
    public class BucketItem
    {
        public int ProductId { get; set; }
        public int Qty { get; set; }
    }
}
