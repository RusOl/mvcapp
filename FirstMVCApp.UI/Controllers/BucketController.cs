﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Brands;
using FirstMVCApp.UI.Models.Buckets;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Buckets;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FirstMVCApp.UI.Controllers
{
    public class BucketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IBucketService _bucketService;
        public readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BucketController(UserManager<User> userManager, IBucketService bucketService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _bucketService = bucketService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User user = await _userManager.GetUserAsync(User);
            var prModel = _bucketService.GetBucket(user);
            return View(prModel);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Add(int? productId)
        {
            if (!productId.HasValue)
            {
                ViewBag.BadRequestMessage = "Product Id can not be NULL";
                return View("BadRequest");
            }

            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                await _bucketService.AddToBucketAsync(productId.Value, currentUser);
                return RedirectToAction("Index", "Product");
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Index(OrderModel model)
        {
            User user = await _userManager.GetUserAsync(User);
            var item = _bucketService.GetBucket(user);
            _bucketService.OrderToBucket(item, user);
            user.Bucket = String.Empty;
            await _userManager.UpdateAsync(user);
            return RedirectToAction("Index", "Product");
        }
    }
}
