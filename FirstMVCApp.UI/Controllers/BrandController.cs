﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.UI.Models.Brands;
using FirstMVCApp.UI.Services.Brands.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService _brandService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public BrandController(IBrandService brandService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));

            _brandService = brandService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brands = unitOfWork.Brands.GetAll().ToList();
                List<BrandModel> brandModels = Mapper.Map<List<BrandModel>>(brands);
                return View(brandModels);
            }
        }

        public IActionResult Create()
        {
            var model = _brandService.GetBrandCreateModel();

            return View(model);
        }
        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}