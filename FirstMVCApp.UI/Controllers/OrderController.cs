﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Brands;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Services.Brands.Contracts;
using FirstMVCApp.UI.Services.Orders.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public readonly UserManager<User> _userManager;
        public OrderController(IOrderService orderService, UserManager<User> userManager )
        {
            if (orderService == null)
                throw new ArgumentNullException(nameof(orderService));

            _orderService = orderService;
            _userManager = userManager;
        }
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User user = await _userManager.GetUserAsync(User);
            var ordersModel = _orderService.GetOrders(user);
            return View(ordersModel);
        }
    }
}
