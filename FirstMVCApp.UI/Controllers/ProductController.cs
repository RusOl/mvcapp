﻿using System;
using FirstMVCApp.UI.Models.Products;
using FirstMVCApp.UI.Services.Products.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Controllers
{
    public class ProductController : Controller
    {
        // DI - dependency injection
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            if(productService == null)
                throw new ArgumentNullException(nameof(productService));

            _productService = productService;
        }

        //action
        [Route("Search/{name?}/{categoryId?}/{brandId?}/{priceFrom?}/{priceTo?}")]
        public IActionResult Index([FromRoute]ProductFilterModel model)
        {
            try
            {
                var models = _productService.SearchProducts(model);

                return View(models);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ViewBag.BadRequestMessage = ex.Message;
                return View("BadRequest");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public IActionResult Create()
        {
            var model = _productService.GetProductCreateModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(ProductCreateModel model)
        {
            try
            {
                _productService.CreateProduct(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}