﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.UI.Models.Categories;
using FirstMVCApp.UI.Services.Categories.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.UI.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public CategoryController(ICategoryService categoryService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            _categoryService = categoryService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = unitOfWork.Categories.GetAll().ToList();
                List<CategoryModel> categoryModels = Mapper.Map<List<CategoryModel>>(categories);
                return View(categoryModels);
            }
        }

        public IActionResult Create()
        {
            var model = _categoryService.GetCategoryCreateModel();

            return View(model);
        }
        [HttpPost]
        public IActionResult Create(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}