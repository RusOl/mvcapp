﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.UI.Models.Brands;
using FirstMVCApp.UI.Models.Categories;
using FirstMVCApp.UI.Models.Orders;
using FirstMVCApp.UI.Models.Products;

namespace FirstMVCApp.UI
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateBrandToBrandModelMap();
            CreateBrandCreateModelToBrand();
            CreateCategoryToCategoryModelMap();
            CreateCategoryCreateModelToCategory();
            CreateOrderToOrderModelMap();
            CreateOrderCreateOrderModelToOrder();
        }

        private void CreateOrderCreateOrderModelToOrder()
        {
            CreateMap<OrderModel, Order>();
        }

        private void CreateOrderToOrderModelMap()
        {
            CreateMap<Order, OrderModel>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }

        private void CreateCategoryToCategoryModelMap()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateBrandToBrandModelMap()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }
    }
}
