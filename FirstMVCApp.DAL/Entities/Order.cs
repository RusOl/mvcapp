﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FirstMVCApp.DAL.Entities
{
    public class Order : Entity
    {
        public int UserId { get; set; }
        public string Product { get; set; }
        public decimal Sum { get; set; }
        public DateTime DT { get; set; }
    }
}
