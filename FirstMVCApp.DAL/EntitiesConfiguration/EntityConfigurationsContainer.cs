﻿using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.EntitiesConfiguration.Contracts;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Product> ProductConfiguration { get; }
        public IEntityConfiguration<Category> CategoryConfiguration { get; }
        public IEntityConfiguration<Brand> BrandConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            ProductConfiguration = new ProductConfiguration();
            CategoryConfiguration = new CategoryConfiguration();
            BrandConfiguration = new BrandConfiguration();
        }
    }
}