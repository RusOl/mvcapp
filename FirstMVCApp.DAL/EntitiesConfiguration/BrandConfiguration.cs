﻿using FirstMVCApp.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FirstMVCApp.DAL.EntitiesConfiguration
{
    public class BrandConfiguration : BaseEntityConfiguration<Brand>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Brand> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .HasIndex(b => b.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Brand> builder)
        {
            builder
                .HasMany(b => b.Products)
                .WithOne(b => b.Brand)
                .HasForeignKey(b => b.BrandId);
        }
    }
}
