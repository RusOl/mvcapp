﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Product> ProductConfiguration { get; }
        IEntityConfiguration<Category> CategoryConfiguration { get; }
        IEntityConfiguration<Brand> BrandConfiguration { get; }

    }
}
