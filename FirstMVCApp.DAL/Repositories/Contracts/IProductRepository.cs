﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetAllWithCategoriesAndBrandsByPrice(decimal priceFrom, decimal priceTo);
        IEnumerable<Product> GetAllWithCategoriesAndBrands();
    }
}
