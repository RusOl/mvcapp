﻿using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface ICategoryRepository : IRepository<Category>
    {
    }

    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Categories;
        }
    }
}
