﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using FirstMVCApp.DAL.Entities;

namespace FirstMVCApp.DAL.Repositories.Contracts
{
    public interface IOrderRepository : IRepository<Order>
    {
        public IEnumerable<Order> GetOrderbyUser(int userId);
    }
}
