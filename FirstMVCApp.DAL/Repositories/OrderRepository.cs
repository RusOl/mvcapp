﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.DAL.Repositories.Contracts;

namespace FirstMVCApp.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Orders;
        }

        public IEnumerable<Order> GetOrderbyUser(int userId)
        { 
            return entities.Where(e => e.UserId == userId);
        }
    }
}
