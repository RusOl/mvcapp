﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FirstMVCApp.DAL.Migrations
{
    public partial class init2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Ords_OrdId1",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_OrdId1",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "OrdId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "OrdId1",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrdId",
                table: "Products",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OrdId1",
                table: "Products",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_OrdId1",
                table: "Products",
                column: "OrdId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Ords_OrdId1",
                table: "Products",
                column: "OrdId1",
                principalTable: "Ords",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
