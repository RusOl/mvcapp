﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Models.Products;
using FirstMVCApp.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class OrdController : Controller
    {
        private readonly IOrdService _ordService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public OrdController(IOrdService ordService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (ordService == null)
                throw new ArgumentNullException(nameof(ordService));

            _ordService = ordService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ords = unitOfWork.Ords.GetProductsForOrds().ToList();
                    List<OrdModel> ordModels = Mapper.Map<List<OrdModel>>(ords);
                    return View(ordModels);
            }
        }
        [HttpGet]
        public IActionResult CreateOrd(ProductModel prod)
        {
            var model = _ordService.GetOrdCreateModel();
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateOrd(OrdCreateModel model)
        {
            try
            {
                _ordService.CreateOrd(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
