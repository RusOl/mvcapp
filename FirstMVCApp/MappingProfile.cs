﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Brands;
using FirstMVCApp.Models.Categories;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Models.Products;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateBrandToBrandModelMap();
            CreateBrandCreateModelToBrand();
            CreateCategoryToCategoryModelMap();
            CreateCategoryCreateModelToCategory();
            CreateOrdCreateModelToOrd();
            CreateOrdModelCreateOrdMap();
        }
        private void CreateOrdModelCreateOrdMap()
        {
            CreateMap<Ord, OrdModel>();
        }
        private void CreateOrdCreateModelToOrd()
        {
            CreateMap<OrdCreateModel, Ord>();
        }

        private void CreateCategoryCreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }

        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }

        private void CreateCategoryToCategoryModelMap()
        {
            CreateMap<Category, CategoryModel>();
        }

        private void CreateBrandToBrandModelMap()
        {
            CreateMap<Brand, BrandModel>();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }
    }
}
