﻿using System;

namespace FirstMVCApp.Models.Orders
{
    public class OrdModel
    {
        public static string NoBrand = "NoName";
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string ProductName { get; set; }
        public string? Commentary { get; set; }
        public int Counter { get; set; }
        public DateTime DateTime { get; set; }
        public decimal PriceAVG { get; set; }
        public int CountOfOrg { get; set; }
        public decimal ProductPrice { get; set; }
    }
}
