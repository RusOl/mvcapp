﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Models.Orders
{
    public class OrdCreateModel
    {
        [Display(Name = "ФИО")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        [Display(Name = "Продукт")]
        [Required(ErrorMessage = "Продукт должнен быть указан")]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        [Display(Name = "Кол-во")]
        [Required(ErrorMessage = "Кол-во должно быть указано")]
        [Range(0, Double.PositiveInfinity, ErrorMessage = "Кол-во не может быть отрицательным")]
        public int Counter { get; set; }

        [Display(Name = "Комментарий")]
        public string Commentary { get; set; }

        [Display(Name = "Время")]
        public DateTime Datetime { get; set; }
        [Display(Name = "Сумма")]
        public decimal PriceAVG { get; set; }
        public SelectList ProductsSelect { get; set; }
        public SelectList ClientsSelect { get; set; }

    }
}
