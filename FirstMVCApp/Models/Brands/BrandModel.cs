﻿namespace FirstMVCApp.Models.Brands
{
    public class BrandModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
