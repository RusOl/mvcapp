﻿using System.ComponentModel.DataAnnotations;

namespace FirstMVCApp.Models.Categories
{
    public class CategoryCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }
    }
}
