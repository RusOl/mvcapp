﻿using System;
using System.Linq;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Orders;
using FirstMVCApp.Services.Orders.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FirstMVCApp.Services.Orders
{
    public class OrdService : IOrdService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrdService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public OrdCreateModel GetOrdCreateModel()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Products.GetAll().ToList();

                return new OrdCreateModel()
                {
                    ProductsSelect = new SelectList(products, nameof(Product.Id), nameof(Product.Name))
                };
            }
        }

        public void CreateOrd(OrdCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var priceProduct = unitOfWork.Products.GetById(model.ProductId).Price;
                model.PriceAVG = model.Counter * priceProduct;
                var nameProduct = unitOfWork.Products.GetById(model.ProductId).Name;
                model.ProductName = nameProduct;
                model.Datetime = DateTime.Now;
                var ords = Mapper.Map<Ord>(model);
                unitOfWork.Ords.Create(ords);
            }
        }
    }
}
