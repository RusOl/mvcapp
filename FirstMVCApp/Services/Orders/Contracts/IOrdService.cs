﻿using FirstMVCApp.Models.Orders;

namespace FirstMVCApp.Services.Orders.Contracts
{
    public interface IOrdService
    {
        OrdCreateModel GetOrdCreateModel();
        void CreateOrd(OrdCreateModel model);
    }
}
