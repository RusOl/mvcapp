﻿using System.Collections.Generic;
using FirstMVCApp.Models.Products;

namespace FirstMVCApp.Services.Products.Contracts
{
    public interface IProductService
    {
        List<ProductModel> SearchProducts(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
    }
}