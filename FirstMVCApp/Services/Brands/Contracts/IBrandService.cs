﻿using FirstMVCApp.Models.Brands;

namespace FirstMVCApp.Services.Brands.Contracts
{
    public interface IBrandService
    {
        BrandCreateModel GetBrandCreateModel();
        void CreateBrand(BrandCreateModel model);
    }
}
