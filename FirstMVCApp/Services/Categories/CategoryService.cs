﻿using System;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models.Categories;
using FirstMVCApp.Services.Categories.Contracts;

namespace FirstMVCApp.Services.Categories
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public CategoryCreateModel GetCategoryCreateModel()
        {
            return new CategoryCreateModel();
        }
        public void CreateCategory(CategoryCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var categories = Mapper.Map<Category>(model);

                unitOfWork.Categories.Create(categories);
            }
        }
    }
}
