﻿using FirstMVCApp.Models.Categories;

namespace FirstMVCApp.Services.Categories.Contracts
{
    public interface ICategoryService
    {
        CategoryCreateModel GetCategoryCreateModel();
        void CreateCategory(CategoryCreateModel model);
    }
}
